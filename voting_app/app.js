const ProductList = React.createClass({
    getInitialState: function () {
        return {
            products: [],
        };
    },

    componentDidMount: function () {
        this.setState({products: Data});
    },

    updateState: function () {
        const products = this.state.products.sort((a, b) => {
            return b.votes - a.votes;
        });
        this.setState({products: products});
    },

    handleProductUpVote: function (productId) {
        Data.forEach((el) => {
            if(el.id === productId) {
                el.votes += 1;
            }
        });
        this.updateState();
    },

    handleProductDownVote: function (productId) {
        Data.forEach((el) => {
            if(el.id === productId) {
                el.votes -= 1;
            }
        });
        this.updateState();
    },

    sort: function (event) {
        const products = this.state.products.sort((a, b) => {
            if (event.target.value === '1') {
                return a.votes - b.votes
            } else if (event.target.value === '0') {
                return b.votes - a.votes
            }
        });
        this.setState({products: products});
    },

    render: function () {
        const products = this.state.products.map((product) => {
            return (
                    <Product
                        key={'product-' + product.id}
                        id={product.id}
                        title={product.title}
                        description={product.description}
                        url={product.url}
                        votes={product.votes}
                        submitter_avatar_url={product.submitter_avatar_url}
                        product_image_url={product.product_image_url}
                        onVote={this.handleProductUpVote}
                        onDownVote={this.handleProductDownVote}
                    />
            );
        });
        return (
            <div >
                <select className="ui dropdown" onChange={this.sort}>
                    <option value="">Sort...</option>
                    <option value="1">Ascending</option>
                    <option value="0">Descending</option>
                </select>
                <div className='ui items'>
                    {products}
                </div>
            </div>
        )
    },
});

const Product = React.createClass({
    handleUpVote: function () {
        this.props.onVote(this.props.id);
    },

    handleDownVote: function () {
        this.props.onDownVote(this.props.id);
    },

    render: function () {
        return (
            <div className='item'>
                <div className='image'>
                    <img src={this.props.product_image_url} />
                </div>
                <div className='middle aligned content'>
                    <div className='header'>
                        <a onClick={this.handleUpVote}>
                            <i className='large caret up icon' />
                        </a>
                        <a onClick={this.handleDownVote}>
                            <i className='large caret down icon' />
                        </a>
                        {this.props.votes}
                    </div>
                    <div className='description'>
                        <a href={this.props.url}>{this.props.title}</a>
                        <p>{this.props.description}</p>
                    </div>
                    <div className='extra'>
                        <span>Submitted by:</span>
                        <img
                        className='ui avatar image'
                        src={this.props.submitter_avatar_url}
                        />
                    </div>
                </div>
            </div>
        );
    }
});

ReactDOM.render(
    <ProductList />,
    document.getElementById('content')
);
