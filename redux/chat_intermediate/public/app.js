const reducer = Redux.combineReducers({
    activeThreadId: activeThreadIdReducer,
    threads: threadsReducer
});

function findThreadIndex(state, action) {
    switch (action.type) {
        case 'ADD_MESSAGE':
            return state.findIndex((t) => t.id === action.id);
        case 'DELETE_MESSAGE':
            return state.findIndex((t) => t.id === action.threadId);
    }
}

function threadsReducer(state = [
    {
        id: '1-fca2',
        title: 'Buzz Aldrin',
        messages: messagesReducer(undefined, {})
    },
    {
        id: '2-be91',
        title: 'Micheal Collins',
        messages: messagesReducer(undefined, {}),
    }
], action) {
    switch (action.type) {
        case 'ADD_MESSAGE':
        case 'DELETE_MESSAGE': {
            const threadIndex = findThreadIndex(state, action);
            const oldThread = state[threadIndex];
            const newThread = {
                ...oldThread,
                messages: messagesReducer(oldThread.messages, action)
            };
            return [
                ...state.slice(0, threadIndex),
                newThread,
                ...state.slice(threadIndex + 1, state.length)
            ];
        }
        default: {
            return state;
        }
    }
}

function messagesReducer(state = [], action) {
    switch (action.type) {
        case 'ADD_MESSAGE': {
            const newMessage = {
                id: uuid.v4(),
                timestamp: Date.now(),
                text: action.text
            };
            return state.concat(newMessage)
        }
        case 'DELETE_MESSAGE': {
            const index = state.findIndex(
                (m) => m.id === action.id
            );
            return [
                ...state.slice(0, index),
                ...state.slice(
                    index + 1, state.length
                ),
            ]
        }
        default:
            return state
    }
}

function activeThreadIdReducer(state = '1-fca2', action) {
    if (action.type === 'CHANGE_TAB') {
        return action.id
    } else {
        return state;
    }
}

const store = Redux.createStore(reducer);

function addMessage(threadId, value) {
    return {
        type: 'ADD_MESSAGE',
        text: value,
        id: threadId
    }
}

function deleteMessage(id, threadId) {
    return {
        type: 'DELETE_MESSAGE',
        threadId: threadId,
        id: id
    }
}

function openThread(id) {
    return {
        type: 'CHANGE_TAB',
        id: id
    }
}

const App = () => (
    <div className='ui segment'>
        <ThreadTabs/>
        <ThreadDisplay/>
    </div>
);

const Tabs = ((props) => (
    <div className="ui top attached tabular menu">
        {
            props.tabs.map((tab, i) => (
                <div
                    key={i}
                    className={tab.active ? 'active item' : 'item'}
                    onClick={() => props.onClick(tab.id)}
                >
                    {tab.title}
                </div>
            ))
        }
    </div>
));

const mapStateToTabsProps = (state) => {
    const tabs = state.threads.map((t) => (
        {
            title: t.title,
            active: t.id === state.activeThreadId,
            id: t.id
        }
    ));

    return {
        tabs,
    }
};

const mapDispatchToTabsProps = (dispatch) => {
    return {
        onClick : (id) => (
            dispatch(openThread(id))
        )
    }
};

const ThreadTabs = ReactRedux.connect(
    mapStateToTabsProps,
    mapDispatchToTabsProps
)(Tabs);


const TextFieldSubmit = ((props) => {
    let input;

    return (
        <div className='ui input'>
            <input
                ref={node => input = node}
                type='text'
            >
            </input>
            <button
                onClick={() => {
                    props.onClick(input.value);
                    input.value = '';
                }}
                className='ui primary button'
                type='submit'
            >
                {props.buttonName}
            </button>
        </div>
    )
});

const MessagesList = (props) => (
    <div className='ui comments'>
        {
            props.messages.map((message, index) => (
                <div
                    className='comment'
                    key={index}
                    onClick={() => props.onClick(message.id)}
                >
                    <div className="text">
                        {message.text}
                        <span className="metadata">@{message.timestamp}</span>
                    </div>
                </div>
            ))
        }
    </div>
);


const mapStateToThreadProps = (state) => {
    const thread = state.threads.find((t) => t.id === state.activeThreadId);
    return {
        thread,
    }
};

const mapDispatchToThreadProps = (dispatch) => {
    return {
        dispatch,
    }
};

const mergeThreadProps = (stateProps, dispatchProps) => {
    return {
        ...stateProps,
        ...dispatchProps,
        onMessageClick : (id) => (
            dispatchProps.dispatch(deleteMessage(id, stateProps.thread.id))
        ),
        onMessageSubmit : (value) => (
            dispatchProps.dispatch(addMessage(stateProps.thread.id, value))
        ),
    }
};

const Thread = ((props) => (
    <div className='ui center aligned basic segment'>
        <MessagesList
            messages={props.thread.messages}
            onClick={props.onMessageClick}
        />
        <TextFieldSubmit
            buttonName="Submit"
            onClick={props.onMessageSubmit}
        />
    </div>
));

const ThreadDisplay = ReactRedux.connect(
    mapStateToThreadProps,
    mapDispatchToThreadProps,
    mergeThreadProps
)(Thread);


ReactDOM.render(
    <ReactRedux.Provider store={store}>
        <App/>
    </ReactRedux.Provider>,
    document.getElementById('content')
);
