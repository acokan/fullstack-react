function createStore(reducer, initialState) {
    let state = initialState;
    const listeners = [];

    const getState = () => (state);

    const dispatch = (action) => {
        state = reducer(state, action);
        listeners.forEach(l => l());
    };

    const subscribe = (listener) => (
        listeners.push(listener)
    );

    return {
        getState,
        dispatch,
        subscribe,
    };
}

function reducer (state, action) {
    if (action.type === 'ADD_MESSAGES') {
        return {
            messages: state.messages.concat(action.message)
        }
    } else if (action.type === 'DELETE_MESSAGES') {
        return {
            messages: [
                ...state.messages.slice(0, action.index),
                ...state.messages.slice(action.index + 1, state.messages.length)
            ]
        }
    } else {
        return state;
    }
}

const initialState = {messages: []};

const store = createStore(reducer, initialState);


class App extends React.Component {

    componentDidMount() {
        store.subscribe(() => this.forceUpdate());
    }

    render() {
        const messages = store.getState().messages;
        return (
            <div className="ui segment">
                <MessageView messages={messages} />
                <MessageInput />
            </div>
        )
    }
}

class MessageView extends React.Component {

    render() {
        const messages = this.props.messages.map((message, i) => (
            <div className="comment"
                 key={i}
                 onClick={() => (
                     store.dispatch({type: 'DELETE_MESSAGES', index: i})
                 )}
            >
                {message}
            </div>
        ));

        return (
            <div className="ui comments">
                {messages}
            </div>
        )
    }
}


class MessageInput extends React.Component {
    render() {
        return (
            <div className="ui input">
                <input type="text" ref="messageInput" />
                <button className="ui primary button"
                        type="submit"
                        onClick={() => {
                            store.dispatch({type: "ADD_MESSAGES", message: this.refs.messageInput.value});
                            this.refs.messageInput.value = '';
                        }}>
                    Submit
                </button>
            </div>
        )
    }
}


ReactDOM.render(<App />, document.getElementById('content'));