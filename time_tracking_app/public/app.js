const TimersDashboard = React.createClass({
    getInitialState: function () {
        return {
            timers: [],
            errorMsg: null
        }
    },

    componentDidMount: function () {
        this.loadTimersFromServer();
        setInterval(this.loadTimersFromServer, 5000);
    },

    loadTimersFromServer: function () {
        client.getTimers((timers) => {
            this.setState({timers: timers});
        })
    },

    handleCreateFormSubmit: function (timer) {
        this.createTimer(timer);
    },

    createTimer: function (timer) {
        const t = helpers.newTimer(timer);
        this.setState({
            timers: this.state.timers.concat(t),
        });
        client.createTimer(t, this.onSuccess, this.onError);
    },

    handleEditSubmitForm: function (attrs) {
        this.updateTimer(attrs);
    },

    handleDeleteTimer: function (timer) {
        this.deleteTimer(timer);
    },

    deleteTimer: function (timerId) {
        this.setState({
            timers: this.state.timers.filter(timer => timer.id !== timerId)
        });
        client.deleteTimer({id: timerId}, this.onSuccess, this.onError);
    },

    updateTimer: function (attrs) {
        this.setState({
            timers: this.state.timers.map((timer) => {
                if (timer.id === attrs.id) {
                    return Object.assign({}, timer, {
                       title: attrs.title,
                       project: attrs.project
                    });
                } else {
                    return timer;
                }
            })
        });
        client.updateTimer(attrs, this.onSuccess, this.onError);
    },

    onSuccess: function () {
        this.setState({errorMsg: null});
    },

    onError: function () {
        this.setState({errorMsg: "Greska u komunikaciji sa serverom!"});
    },

    handleStartClick: function (timerId) {
        this.startTimer(timerId);
    },

    handleStopClick: function (timerId) {
        this.stopTimer(timerId);
    },

    startTimer: function (timerId) {
        const now = Date.now();
        this.setState({
            timers: this.state.timers.map((timer) => {
                if (timer.id === timerId) {
                    return Object.assign({}, timer, {
                       runningSince: now
                    });
                } else {
                    return timer;
                }
            })
        });
        client.startTimer({
            id: timerId,
            start: now
        });
    },

    stopTimer: function (timerId) {
        const now = Date.now();
        this.setState({
            timers: this.state.timers.map((timer) => {
                const lastElapsed = now - timer.runningSince;
                if (timer.id === timerId) {
                    return Object.assign({}, timer, {
                        elapsed: timer.elapsed + lastElapsed,
                        runningSince: null
                    });
                } else {
                    return timer;
                }
            })
        });
        client.stopTimer({
            id: timerId,
            stop: now
        })
    },

    render: function () {
        return (
            <div>
                <ErrorMessages
                    errorMsg={this.state.errorMsg}
                />
                <div className='ui three column centered grid'>
                    <div className='column'>
                        <EditableTimerList
                            timers={this.state.timers}
                            onFormSubmit={this.handleEditSubmitForm}
                            onDeleteTimer={this.handleDeleteTimer}
                            onStartClick={this.handleStartClick}
                            onStopClick={this.handleStopClick}
                            errorMsg={this.state.errorMsg}
                        />
                        <ToggleableTimerForm
                            onFormSubmit={this.handleCreateFormSubmit}
                        />
                    </div>
                </div>
            </div>
        );
    },
});

const EditableTimerList = React.createClass({
   render: function () {
       const timers = this.props.timers.map((timer) => (
           <EditableTimer
                id={timer.id}
                key={timer.id}
                title={timer.title}
                project={timer.project}
                elapsed={timer.elapsed}
                runningSince={timer.runningSince}
                onFormSubmit={this.props.onFormSubmit}
                onDeleteTimer={this.props.onDeleteTimer}
                onStartClick={this.props.onStartClick}
                onStopClick={this.props.onStopClick}
                errorMsg={this.props.errorMsg}
            />
       ));
       return (
           <div className='timers'>
               {timers}
           </div>
       );
   },
});

const EditableTimer = React.createClass({
    getInitialState: function () {
        return {
            editFormOpen: false
        }
    },

    handleEditForm: function () {
        this.openEditForm();
    },

    openEditForm: function () {
        this.setState({editFormOpen: true});
    },

    handleCloseForm: function () {
        this.closeForm();
    },

    closeForm: function () {
        this.setState({editFormOpen: false});
    },

    handleSubmitForm: function (timer) {
        this.props.onFormSubmit(timer);
        setTimeout(() => {
            !!this.props.errorMsg ? this.openEditForm() : this.closeForm();
        }, 50)

    },

    render: function () {

        if (this.state.editFormOpen) {
            return (
                <TimerForm
                    id={this.props.id}
                    title={this.props.title}
                    project={this.props.project}
                    onFormSubmit={this.handleSubmitForm}
                    onFormClose={this.handleCloseForm}
                    errorMsg={this.props.errorMsg}
                />
            );
        } else {
            return (
                <Timer
                    id={this.props.id}
                    title={this.props.title}
                    project={this.props.project}
                    elapsed={this.props.elapsed}
                    runningSince={this.props.runningSince}
                    onEditForm={this.handleEditForm}
                    onDeleteTimer={this.props.onDeleteTimer}
                    onStartClick={this.props.onStartClick}
                    onStopClick={this.props.onStopClick}
                />
            );
        }
    },
});

const TimerForm = React.createClass({
    // getInitialState: function () {
    //     return {
    //         error: '',
    //         areErrors: false
    //     }
    // },
    handleSubmit: function () {
        this.props.onFormSubmit({
            id: this.props.id,
            title: this.refs.title.value,
            project: this.refs.project.value
        });

        // const title = this.refs.title.value;
        // const project = this.refs.project.value;
        //
        // if (title === '' || project === '') {
        //     this.setState({
        //         error: "Fields must not be empty!",
        //         areErrors: true
        //     })
        // } else {
        //     this.props.onFormSubmit({
        //         id: this.props.id,
        //         title: title,
        //         project: project
        //     })
        // }
    },

    render: function () {
        const submitText = this.props.id ? 'Update' : 'Create';
        return (
            <div className='ui centered card'>
                <div className='content'>
                    <div className='ui form' ref='form'>
                        <div className='field'>
                            <label>Title</label>
                            <input type='text' ref='title' defaultValue={this.props.title} />
                        </div>
                        <div className='field'>
                            <label>Project</label>
                            <input type='text' ref='project' defaultValue={this.props.project} />
                        </div>
                        <div className='ui two bottom attached buttons'>
                            <button className='ui basic blue button' onClick={this.handleSubmit} >
                                {submitText}
                            </button>
                            <button className='ui basic red button' onClick={this.props.onFormClose}>
                                Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    },
});

const ToggleableTimerForm = React.createClass({
    getInitialState: function () {
        return {
            isOpen: false
        }
    },

    handleOpenForm: function () {
        this.setState({isOpen: true});
    },

    handleFormSubmit: function (timer) {
        this.props.onFormSubmit(timer);
        this.setState({isOpen: false});
    },

    handleFormClose: function () {
        this.setState({isOpen: false});
    },

    render: function () {
        if (this.state.isOpen) {
            return (
                <TimerForm
                    onFormSubmit={this.handleFormSubmit}
                    onFormClose={this.handleFormClose}
                />
            );
        } else {
            return (
                <div className='ui basic content center aligned segment'>
                    <button
                        className='ui basic button icon'
                        onClick={this.handleOpenForm}
                    >
                        <i className='plus icon' />
                    </button>
                </div>
            );
        }
    }
});

const Timer = React.createClass({

    getInitialState: function () {
        return {
            areControlsShown: false
        }
    },

    componentDidMount: function () {
        this.forceUpdateInterval = setInterval(() => this.forceUpdate(), 50);
    },

    componentWillUnmount: function () {
        clearInterval(this.forceUpdateInterval);
    },

    handleDeleteTimer: function () {
        this.props.onDeleteTimer(this.props.id);
    },

    handleStartClick: function () {
        this.props.onStartClick(this.props.id);
    },

    handleStopClick: function () {
        this.props.onStopClick(this.props.id);
    },

    showIcons: function () {
        this.setState({areControlsShown: true});
    },

    hideIcons: function () {
        this.setState({areControlsShown: false});
    },

    render: function () {
        const elapsedString = helpers.renderElapsedString(this.props.elapsed, this.props.runningSince);

        return (
            <div className='ui centered card'
                 onMouseEnter={this.showIcons}
                 onMouseLeave={this.hideIcons}
            >
                <div className='content'>
                    <div className='header'>
                        {this.props.title}
                    </div>
                    <div className='meta'>
                        {this.props.project}
                    </div>
                    <div className='center aligned description'>
                        <h2>
                            {elapsedString}
                        </h2>
                    </div>
                    <ControlButtons
                        areControlsShown={this.state.areControlsShown}
                        onEditForm={this.props.onEditForm}
                        onDeleteTimer={this.handleDeleteTimer}
                    />
                </div>
                <TimerActionButton
                    timerIsRunning={!!this.props.runningSince}
                    onStartClick={this.handleStartClick}
                    onStopClick={this.handleStopClick}
                />
            </div>
        );
    },
});

const ControlButtons = React.createClass({
    render: function() {
        if (this.props.areControlsShown) {
            return (
                <div className='extra content'>
                    <span
                        className='right floated edit icon'
                        onClick={this.props.onEditForm}
                    >
                        <i className='edit icon' />
                    </span>
                    <span
                        className='right floated trash icon'
                        onClick={this.props.onDeleteTimer}
                    >
                        <i className='trash icon' />
                    </span>
                </div>
            );
        } else {
            return (
                <div className='extra content'>
                    <i className='icon' />
                </div>
            );
        }
    }
});

const TimerActionButton = React.createClass({
    render: function () {
        const actionButton = this.props.timerIsRunning ? 'Stop' : 'Start',
            buttonClass = this.props.timerIsRunning ? 'red' : 'green',
            actionClick = this.props.timerIsRunning ? this.props.onStopClick : this.props.onStartClick;

        return (
            <div
                className={'ui bottom attached basic button ' + buttonClass}
                onClick={actionClick}
            >
                {actionButton}
            </div>
        );
    }
});

const ErrorMessages = React.createClass({
   render: function () {
       if (!!this.props.errorMsg) {
           return (
               <div className="ui ui three column centered grid">
                   <div className="ui error message">
                       {this.props.errorMsg}
                   </div>
               </div>
           );
       } else {
           return (
               <div>
               </div>
           )
       }
   }
});

ReactDOM.render(
    <TimersDashboard/>,
    document.getElementById('content')
);