import React from 'react';
import isEmail from 'validator/lib/isEmail';
import * as PropTypes from "react/lib/ReactPropTypes";
import Core from './api/core.json';
import Electives from './api/electives.json';

const content = document.createElement('div');
document.body.appendChild(content);

const Courses = {
    core: Core,
    electives: Electives
};

let apiClient;

module.exports = React.createClass({
    displayName: __filename.split('/').slice(-1)[0],

    getInitialState() {
        return {
            fields: {},
            fieldErrors: {},
            people: [],
            _loading: false,
            _saveStatus: 'READY'
        };
    },

    componentWillMount() {
        this.setState({_loading: true});
        apiClient.loadPeople().then((people) => {
           this.setState({_loading: false, people: people});
        });
    },

    onFormSubmit(evt) {
        const person = this.state.fields;

        evt.preventDefault();

        if (this.validate()) return;

        const people = [...this.state.people, person];

        this.setState({_saveStatus: 'SAVING'});
        apiClient.savePeople(people)
            .then(() => {
                this.setState({
                   people: people,
                   fields: {},
                   _saveStatus: 'SUCCESS'
                });
            })
            .catch((err) => {
                console.log(err);
                this.setState({_saveStatus: 'ERROR'});
            });
    },

    onInputChange({name, value, error}) {
        const fields = this.state.fields;
        const fieldErrors = this.state.fieldErrors;

        fields[name] = value;
        fieldErrors[name] = error;

        this.setState({fields, fieldErrors, _saveStatus: 'READY'});
    },

    validate() {
        const person = this.state.fields;
        const fieldErrors = this.state.fieldErrors;
        const errMessages = Object.keys(fieldErrors).filter((k) => fieldErrors[k]);

        if (!person.name) return true;
        if (!person.email) return true;
        if (!person.course) return true;
        if (!person.department) return true;
        if (errMessages.length) return true;

        return false;
    },

    renderPeople() {
        if (this.state._loading) {
            return (
                <img alt='loading' src='/img/loading.gif' />
            )
        }
        return (this.state.people.map(({name, email, department, course}, i) =>
            <li key={i}>{[name, email, department, course].join(' - ')}</li>
        ))
    },

    render() {
        return (
            <div>
                <h1>Sign Up Sheet</h1>
                <form onSubmit={this.onFormSubmit}>

                    <Field
                        placeholder='Name'
                        name='name'
                        value={this.state.fields.name}
                        validate={(val) => val ? false : 'Name is required!'}
                        onChange={this.onInputChange}
                    />

                    <br/>

                    <Field
                        placeholder='Email'
                        name='email'
                        value={this.state.fields.email}
                        validate={(val) => isEmail(val) ? false : 'Invalid Email!'}
                        onChange={this.onInputChange}
                    />

                    <br/>

                    <CourseSelect
                        department={this.state.fields.department}
                        course={this.state.fields.course}
                        onChange={this.onInputChange}
                    />

                    <br/>

                    {{
                        SAVING: <input value='Saving...' type='submit' disabled />,
                        SUCCESS: <input value='Saved!' type='submit' disabled />,
                        ERROR: <input value='Save Failed - Retry?' type='submit' disabled={this.validate()} />,
                        READY: <input value='Submit' type='submit' disabled={this.validate()} />,
                    }[this.state._saveStatus]}

                </form>

                <div>
                    <h3>People</h3>
                    <ul>
                        {this.renderPeople()}
                    </ul>
                </div>
            </div>
        );
    },
});



const CourseSelect = React.createClass({
    propTypes: {
        department: PropTypes.string,
        course: PropTypes.string,
        onChange: PropTypes.func.isRequired
    },

    getInitialState() {
        return {
            department: null,
            course: null,
            courses: [],
            _loading: false
        }
    },

    componentWillReceiveProps(update) {
        this.setState({
            department: update.department,
            course: update.course
        })
    },

    fetch(department) {
        this.setState({_loading: true, courses: []});
        apiClient.loadCourses(department).then((courses) => {
            this.setState({_loading: false, courses: courses});
        })
    },

    onSelectDepartment(e) {
        const department = e.target.value;
        const course = null;
        this.setState({department, course});
        this.props.onChange({name: 'department', value: department});
        this.props.onChange({name: 'course', value: course});

        if (department) this.fetch(department);
    },

    onSelectCourse(e) {
        const course = e.target.value;
        this.setState({course});
        this.props.onChange({name: 'course', value: course});
    },

    renderDepartmentSelect() {
        return(
            <select value={this.state.department || ''} onChange={this.onSelectDepartment} >
                <option value=''>Which department?</option>
                <option value='core'>NodeSchool: Core</option>
                <option value='electives'>NodeSchool: Electives</option>
            </select>
        )
    },

    renderCourseSelect() {
        if (this.state._loading) {
            return (
                <img alt='loading' src='/img/loading.gif' />
            )
        }
        if (!this.state.department) {
            return(
                <span/>
            )
        }
        return (
            <select value={this.state.course || ''} onChange={this.onSelectCourse}>
                {[
                    <option value='' key='course-none'>Which course?</option>,
                    ...this.state.courses.map((val, i) => (
                        <option key={i} value={val}>
                            {val}
                        </option>
                    ))
                ]}
            </select>
        )

    },

    render() {
        return(
            <div>
                { this.renderDepartmentSelect() }
                <br />
                { this.renderCourseSelect() }
            </div>
        );
    }

});

const Field = React.createClass({
    propTypes: {
        placeholder: PropTypes.string,
        name: PropTypes.string.isRequired,
        value: PropTypes.string,
        validate: PropTypes.func,
        onChange: PropTypes.func.isRequired
    },

    getInitialState() {
        return {
            value: this.props.value,
            error: false
        }
    },

    componentWillReceiveProps(update) {
        this.setState({value: update.value});
    },

    onChange(e) {
        const name = this.props.name;
        const value = e.target.value;
        const error = this.props.validate ? this.props.validate(value) : false;

        this.setState({value, error});
        this.props.onChange({name, value, error});
    },

    render() {
        return (
            <div>
                <input
                    placeholder={this.props.placeholder}
                    value={this.state.value}
                    onChange={this.onChange}
                />
                <span style={{color: 'red'}}>{this.state.error}</span>
            </div>
        )
    }
});

apiClient = {
    count: 1,
    loadCourses: function (department) {
        return {
            then: function(cb) {
                setTimeout(() => {
                    cb(Courses[department]);
                }, 1000);
            }
        };
    },
    loadPeople: function () {
        return {
            then: function (cb) {
                setTimeout(() => {
                    cb(JSON.parse(localStorage.people || '[]'))
                }, 1000)
            }
        };
    },
    savePeople: function (people) {
        const success = !!(this.count++ %2);

        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if(!success) return reject({success});

                localStorage.people = JSON.stringify(people);
                return resolve({success});
            }, 1000);
        })
    }
};
